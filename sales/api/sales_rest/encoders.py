from common.json import ModelEncoder

from .models import AutomobileVO, Employee, Customer, Sale

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "model",
        "import_href",
    ]

class EmployeeEncoder(ModelEncoder):
    model = Employee
    properties = [
        "employee_name",
        "employee_number",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "customer_name",
        "address",
        "phone_number",
        "id",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "customer",
        "vin",
        "price",
        "id",
    ]

    def get_extra_data(self, o):
        return {
            "employee_name":o.employee.employee_name,
            "employee_number":o.employee.employee_number,
            "customer": o.customer.customer_name,
            "vin": o.vin.vin,
        }

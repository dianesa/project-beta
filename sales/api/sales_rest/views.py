from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Employee, Customer, Sale, AutomobileVO
from .encoders import EmployeeEncoder, CustomerEncoder, SaleEncoder
import json

@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder = SaleEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            employee = Employee.objects.get(employee_name=content["employee"])
            content["employee"] = employee
        except Employee.DoesNotExist:
            return JsonResponse(
                {"message": "Invaild employee name"},
                status=404,
            )

        try:
            customer = Customer.objects.get(customer_name=content["customer"])
            content["customer"] = customer
        except:
            return JsonResponse(
                {"message": "Invaild customer name"},
                status=404,
            )

        try:
            vin = AutomobileVO.objects.get(vin=content["vin"])
            content["vin"] = vin
        except:
            return JsonResponse(
                {"message": "Invalid VIN"},
                status=404,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_sale(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        if "customer" in content:
            try:
                customer = Customer.objects.get(customer_name=content["customer"])
                content["customer"] = customer
            except Customer.DoesNotExist:
                return JsonResponse(
                    {"message": "Invaild customer name"},
                    status=400,
                )

        if "employee" in content:
            try:
                employee = Employee.objects.get(employee_name=content["employee"])
                content["employee"] = employee
            except Employee.DoesNotExist:
                return JsonResponse(
                    {"message": "Invaild employee name"},
                    status=400,
                )

        if "vin" in content:
            try:
                vin = AutomobileVO.objects.get(vin=content["vin"])
                content["vin"] = vin
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invaild VIN"},
                    status=400,
                )

        Sale.objects.filter(id=id).update(**content)
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_employees(request):
    if request.method == "GET":
        employees = Employee.objects.all()
        return JsonResponse(
            {"employees": employees},
            encoder = EmployeeEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        employee = Employee.objects.create(**content)
        return JsonResponse(
            employee,
            encoder=EmployeeEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT"])
def api_employee(request, id):
    if request.method == "GET":
        employee = Employee.objects.get(id=id)
        return JsonResponse(
            employee,
            encoder=EmployeeEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        Employee.objects.filter(id=id).update(**content)
        employee = Employee.objects.get(id=id)
        return JsonResponse(
            employee,
            encoder=EmployeeEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_employee_sales(request, id):
    if request.method == "GET":
        sales = Sale.objects.filter(employee=id)
        return JsonResponse(
            {"sales": sales},
            encoder = SaleEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT"])
def api_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

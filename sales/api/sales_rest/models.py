from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    model = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin

class Employee(models.Model):
    employee_name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=8)

    def __str__(self):
        return self.employee_name

class Customer(models.Model):
    customer_name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return self.customer_name

class Sale(models.Model):
    employee = models.ForeignKey(
        Employee,
        related_name="sales",
        on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.PROTECT,
    )

    vin = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.PROTECT,
    )

    price = models.CharField(max_length=20)


    def __str__(self):
        return self.customer.customer_name

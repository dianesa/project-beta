import React from "react";

class TechnicianForm extends React.Component {
  state = {
    name: "",
    employeeNumber: "",
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify({
        name: this.state.name,
        employee_number: this.state.employeeNumber,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      await response.json();
      const cleared = {
        name: "",
        employeeNumber: "",
      };
      this.setState(cleared);
    }
  };

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value,
    });
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new technician</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Technician name"
                  type="text"
                  name="name"
                  id="name"
                  value={this.state.name}
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Employee Number"
                  type="text"
                  name="employeeNumber"
                  id="employeeNumber"
                  value={this.state.employeeNumber}
                  className="form-control"
                />
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
export default TechnicianForm;

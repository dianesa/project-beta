import React from "react";

class CustomerForm extends React.Component {
  state = {
    customerName: "",
    phoneNumber: "",
    address: "",
  };

  handleInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    const customerUrl = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({
        customer_name: this.state.customerName,
        phone_number: this.state.phoneNumber,
        address: this.state.address,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        customerName: "",
        phoneNumber: "",
        address: "",
      };
      this.setState(cleared);
    }
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Customer</h1>
            <form onSubmit={this.handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Name"
                  type="text"
                  name="customerName"
                  id="customerName"
                  value={this.state.customerName}
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Phone Number"
                  type="text"
                  name="phoneNumber"
                  id="phoneNumber"
                  value={this.state.phoneNumber}
                  className="form-control"
                />
                <label htmlFor="name">Phone Number</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Address"
                  type="text"
                  name="address"
                  id="address"
                  value={this.state.address}
                  className="form-control"
                />
                <label htmlFor="name">Address</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CustomerForm;

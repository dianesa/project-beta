import React from "react";

class SalesPersonForm extends React.Component {
  state = {
    employeeName: "",
    employeeNumber: "",
  };

  handleInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    const employeeUrl = "http://localhost:8090/api/employees/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({
        employee_name: this.state.employeeName,
        employee_number: this.state.employeeNumber,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(employeeUrl, fetchConfig);
    if (response.ok) {
      const cleared = {
        employeeName: "",
        employeeNumber: "",
      };
      this.setState(cleared);
    }
  };

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Sales Person</h1>
            <form onSubmit={this.handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Employee Name"
                  type="text"
                  name="employeeName"
                  id="employeeName"
                  value={this.state.employeeName}
                  className="form-control"
                />
                <label htmlFor="name">Employee Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  placeholder="Employee Number"
                  type="text"
                  name="employeeNumber"
                  id="employeeNumber"
                  value={this.state.employeeNumber}
                  className="form-control"
                />
                <label htmlFor="name">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default SalesPersonForm;

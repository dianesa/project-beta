import React from "react";

class AutomobilesList extends React.Component {
  state = { automobiles: [] };

  async componentDidMount() {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ automobiles: data.autos });
    }
  }
  render() {
    return (
      <div>
        <h1 className="header mt-4">Vehicles List</h1>
        <table className="table">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
            </tr>
          </thead>
          <tbody>
            {this.state.automobiles?.map((automobile) => {
              return (
                <tr key={automobile.vin}>
                  <td>{automobile.vin}</td>
                  <td>{automobile.color}</td>
                  <td>{automobile.year}</td>
                  <td>{automobile.model.name}</td>
                  <td>{automobile.model.manufacturer.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
export default AutomobilesList;
